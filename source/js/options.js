
$(function () {
  $(".flipster").flipster({
    style: 'carousel',
    start: 'center',
    enableMousewheel: false,
    enableNavButtons: true
  });

});
$('.nav__toggle').click(function(){
  $('.nav__items').toggleClass('nav__items--toggle');
});
$("#clients-review").touchCarousel({
  pagingNav: true,
  scrollbar: false,
  directionNavAutoHide: false,
  itemsPerMove: 1,
  loopItems: true,
  directionNav: true,
  autoplay: false,
  autoplayDelay: 2000,
  useWebkit3d: true,
  transitionSpeed: 400
});

// Google
var map;
function initialize() {
  var mapOptions = {
    zoom: 17,
    scrollwheel: false,
    navigationControl: false,
    mapTypeControl: false,
    scaleControl: false,
    center: new google.maps.LatLng(47.227014, 39.710818),
    disableDefaultUI: true
  };



  map = new google.maps.Map(document.getElementById('map'),
      mapOptions);
  var image = 'i/map-marker.png';
  var myLatLng = new google.maps.LatLng(47.227014, 39.710818);
  var beachMarker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    icon: image
  });
}

google.maps.event.addDomListener(window, 'load', initialize);

//

equalheight = function(container){

  var currentTallest = 0,
      currentRowStart = 0,
      rowDivs = new Array(),
      $el,
      topPosition = 0;
  $(container).each(function() {

    $el = $(this);
    $($el).height('auto')
    topPostion = $el.position().top;

    if (currentRowStart != topPostion) {
      for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
        rowDivs[currentDiv].height(currentTallest);
      }
      rowDivs.length = 0; // empty the array
      currentRowStart = topPostion;
      currentTallest = $el.height();
      rowDivs.push($el);
    } else {
      rowDivs.push($el);
      currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
    }
    for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
      rowDivs[currentDiv].height(currentTallest);
    }
  });
}

$(window).load(function() {
  equalheight('.studio__col');
});


$(window).resize(function(){
  equalheight('.studio__col');
});